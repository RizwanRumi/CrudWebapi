﻿app.controller('EmployeeController', ['$scope', '$http',function($scope, $http) {

    $scope.PageTitle = "Employee Crud";
    $scope.employeeList = [];

    $scope.states = {
        showEmpForm : false
    };

    $scope.new = {
        Employee: {}
    };

    $scope.showEmpForm = function (show) {
        $scope.states.showEmpForm = show;
    };
    
    $http.get("/api/Employees/").success(function (data) {
        $scope.employeeList = data;
    }).error(function (e) {
        alert("Server Error!");
    });

    $scope.addEmployee = function () {
        $http.post("/api/Employees/", $scope.new.Employee).success(function (data) {
            $scope.employeeList.push(data);
            $scope.states.showEmpForm = false;
            $scope.new.Employee = {};
        }).error(function(e) { alert("Server Error!"); });
    };

    $scope.getEmployee = function (id) {
        $http.get("/api/Employees/" + id).success(function (data) {
            $scope.emp = data;
        }).error(function (e) {
            alert("Server Error!");
        });
    };

    $scope.editEmployee = function (item) {
        $http.put("/api/Employees/"+item.Id,item).success(function () {
          
        }).error(function (e) { alert("Server Error!"); });
    };
    $scope.deleteEmployee = function (index,deletedId) {
        $http.delete("/api/Employees/" + deletedId).success(function (data) {
            $scope.employeeList.splice(index, 1);
        }).error(function (e) { alert("Server Error!"); });
    };
}]);