﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace CrudWebApi.Models
{
    public class CrudWebApiContext : DbContext
    {
        public CrudWebApiContext() : base("CrudContext")
        {
             
        }

        public DbSet<Employee> Employees { get; set; }
        
    }
}