using CrudWebApi.Models;

namespace CrudWebApi.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CrudWebApi.Models.CrudWebApiContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CrudWebApi.Models.CrudWebApiContext context)
        {
            context.Employees.AddOrUpdate(x=>x.Id,
                new Employee(){Id = 1, EmpName = "Rahul", Designation = "Adplay-Team Lead (SE)"},
                new Employee(){Id = 2, EmpName = "Rizwan", Designation = "Adplay-Asp.Net (SE)" },
                new Employee(){Id = 3, EmpName = "Ratna", Designation = "Adplay-Php (SE)"},
                new Employee(){Id = 4, EmpName = "Foysal", Designation = "Adplay-Android (SE)"},
                new Employee(){Id = 5, EmpName = "Nazmul", Designation = "GpGameStore-Team Lead (SE)"});
        }
    }
}
